﻿using UnityEngine;
using System.Collections;

public class Rope : MonoBehaviour {

    private DistanceJoint2D joint;

	void Start () {
        joint = gameObject.GetComponent<DistanceJoint2D>();
	}
	
    void Update() {
        Debug.DrawLine(joint.connectedAnchor, transform.position, Color.red);

        if (Input.GetButtonDown("Fire1")) {
            Vector2 mousePositionWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float angle = Vector2.Angle(transform.position, mousePositionWorld);
            Debug.Log(angle);
            Vector2 direction = mousePositionWorld - (Vector2)transform.position;
            RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, direction, 10);
            Debug.DrawRay(transform.position, direction, Color.green, 10);
            if (raycastHit.collider != null) {
                joint.connectedAnchor = raycastHit.point;
                joint.distance = raycastHit.distance;
            }
        }
    }
}
